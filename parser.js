function parse (data, conn) {
	var info = data.split(' ');
	var head = info[0];
	var command = info[1];
	var msg = info[3];

	var nick = (head.indexOf('!') != -1) ? head.split('!')[0].replace(':', '') : '';

	switch(head) {
		case 'PING':
		conn.write('PONG\r\n');
		break;
	}

	switch(command) {
		case '001':
		conn.write('JOIN #devsm\r\n');
		conn.write('PRIVMSG #devsm :Ya llegue niñas\r\n');
		break;

		case 'PRIVMSG':
		if (msg.indexOf('@ping') != -1) {
			conn.write('PRIVMSG #devsm :@pong ' + nick + '\r\n')
		}

		if (msg.indexOf('@loved') != -1) {
			conn.write('PRIVMSG #devsm :Dont love here ' + nick + '\r\n')
		}
		break;
	}
}

exports.parse = parse;