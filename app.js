var net = require('net');
var iden = require('./identify.js');
var parser = require('./parser');

var host = 'irc.freenode.net',
    port = 6667;
    
var conn = net.createConnection(port, host);

iden.sendHeaders(conn);

conn.on('data', function (data) {
	parser.parse(data.toString(), conn);

	console.log(data.toString());
});

conn.on('close', function () {
	console.log('Connection lost =/');
});
